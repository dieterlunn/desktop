﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using Fclp;
using TheAirline.Db;
using TheAirline.Infrastructure.Enums;
using TheAirline.Models.General;
using TheAirline.Models.General.Countries;
using Continents = TheAirline.Migrations.Seeds.Continents;

namespace LoadAirlineDb
{
    internal class Program
    {
        private static bool SilentMode { get; set; }
        private static bool LoadSettings { get; set; }
        private static bool LoadAirlines { get; set; }
        private static bool LoadContinents { get; set; }
        private static bool LoadRegions { get; set; }
        private static bool LoadDifficulties { get; set; }
        private static bool All { get; set; }

        private static void Main(string[] args)
        {
            var parser = new FluentCommandLineParser();
            parser.Setup<bool>('a', "All").Callback(all => All = all).SetDefault(false);
            parser.Setup<bool>("settings").Callback(settings => LoadSettings = settings).SetDefault(false);
            parser.Setup<bool>("continents").Callback(continents => LoadContinents = continents).SetDefault(false);
            parser.Setup<bool>("airlines").Callback(airlines => LoadAirlines = airlines).SetDefault(false);
            parser.Setup<bool>("regions").Callback(regions => LoadRegions = regions).SetDefault(false);
            parser.Setup<bool>('s', "silent").Callback(silent => SilentMode = silent).SetDefault(false);
            parser.Setup<bool>("difficulties").Callback(diff => LoadDifficulties = diff).SetDefault(false);
            parser.Parse(args);

            if (LoadSettings || All) LoadGameSettings();
            //if (LoadAirlines || All) LoadAirlineXml();
            if (LoadContinents || All) LoadGameContinents();
            if (LoadRegions || All) LoadGameRegions();
            if (LoadDifficulties || All) LoadDifficultyXml();
        }

        private static void LoadGameSettings()
        {
            if (!SilentMode) Console.WriteLine(@"Loading Settings...");

            var settings = new Settings {Mode = ScreenMode.Windowed};
            var context = new AirlineContext();

            context.Settings.Add(settings);
            context.SaveChanges();

            context.Dispose();

            if (!SilentMode) Console.WriteLine(@"Finished loading settings");
        }

        private static void LoadGameContinents()
        {
            if (!SilentMode) Console.WriteLine(@"Loading Continents...");

            var context = new AirlineContext();
            context.Continents.AddRange(Continents.AllContinents);
            context.SaveChanges();

            context.Dispose();

            if (!SilentMode) Console.WriteLine(@"Finished loading continents");
        }

        private static void LoadGameRegions()
        {
            if (!SilentMode) Console.WriteLine(@"Loading Regions...");

            var regionXml = typeof(Program).Assembly.GetManifestResourceStream("LoadAirlineDb.Data.Regions.xml");

            var xml = new XmlDocument();
            if (regionXml != null) xml.Load(regionXml);

            var root = xml.DocumentElement;

            using (var context = new AirlineContext())
            {
                var regionsList = root?.SelectNodes("//region");
                if (regionsList != null)
                    foreach (XmlElement region in regionsList)
                    {
                        var name = region?.SelectSingleNode("translations//en-US")?.Attributes?["name"].Value;
                        var fuel = Convert.ToDouble(region?.Attributes["fuel"].Value);
                        var uid = region?.Attributes["uid"].Value;
                        var regionContinent = region?.Attributes["continent"].Value;

                        var temp = from cont in context.Continents
                            where cont.Name == regionContinent
                            select cont;

                        var continent = temp.First();

                        if (!SilentMode)
                            Console.WriteLine(@"Adding region " + name + @" to continent " + continent.Name);

                        var value = new Region {Name = name, FuelIndex = fuel, Uid = uid, Continent = continent};

                        context.Regions.Add(value);
                    }

                context.SaveChanges();
            }

            regionXml?.Close();

            if (!SilentMode) Console.WriteLine(@"Finished loading regions");
        }

        private static void LoadAirlineXml()
        {
            if (!SilentMode) Console.WriteLine(@"Loading Airlines...");

            var airlineDocs = Directory.EnumerateFiles("Data/Airlines");

            foreach (var airlineDoc in airlineDocs)
            {
                var xml = new XmlDocument();
                xml.Load(airlineDoc);
            }
        }

        private static void LoadDifficultyXml()
        {
            if(!SilentMode) Console.WriteLine(@"Loading Difficulties...");

            var diffXml = typeof(Program).Assembly.GetManifestResourceStream("LoadAirlineDb.Data.Difficulties.xml");

            var xml = new XmlDocument();
            if (diffXml != null) xml.Load(diffXml);

            using (var context = new AirlineContext())
            {
                var root = xml.DocumentElement;

                var diffList = root?.SelectNodes("//difficulty");

                if (diffList != null)
                    foreach (XmlElement difficulty in diffList)
                    {
                        var name = difficulty.InnerText.Trim();
                        var ailevel = Convert.ToDouble(difficulty.Attributes["ailevel"].Value);
                        var loanlevel = Convert.ToDouble(difficulty.Attributes["loanlevel"].Value);
                        var moneylevel = Convert.ToDouble(difficulty.Attributes["moneylevel"].Value);
                        var passengerlevel = Convert.ToDouble(difficulty.Attributes["passengerlevel"].Value);
                        var pricelevel = Convert.ToDouble(difficulty.Attributes["pricelevel"].Value);
                        var startdatalevel = Convert.ToDouble(difficulty.Attributes["startdatalevel"].Value);

                        if (!SilentMode) Console.WriteLine(@"Adding " + name + @" difficulty");

                        context.Difficulties.Add(new Difficulty {AiLevel = ailevel, LoanLevel = loanlevel, MoneyLevel = moneylevel, PassengersLevel = passengerlevel, Name = name, PriceLevel = pricelevel, StartDataLevel = startdatalevel});
                    }

                context.SaveChanges();
            }

            diffXml?.Close();

            if (!SilentMode) Console.WriteLine(@"Finished adding difficulties");
        }
    }
}