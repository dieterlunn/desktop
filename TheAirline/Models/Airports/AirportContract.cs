﻿using System;
using TheAirline.Helpers;
using TheAirline.Infrastructure.Enums;
using TheAirline.Models.Airlines;
using TheAirline.Models.General;

namespace TheAirline.Models.Airports
{
    //the class for a contract at an airport for an airline
    public class AirportContract
    {
        #region Constructors and Destructors

        public AirportContract(
            Airline airline,
            Airport airport,
            ContractType type,
            TerminalType terminaltype,
            DateTime date,
            int numberOfGates,
            int length,
            double yearlyPayment,
            bool autorenew,
            bool payFull = false,
            bool isExclusiveDeal = false,
            Terminal terminal = null)
        {
            Type = type;
            PayFull = payFull;
            Airline = airline;
            Airport = airport;
            ContractDate = date;
            Length = length;
            YearlyPayment = yearlyPayment;
            NumberOfGates = numberOfGates;
            IsExclusiveDeal = isExclusiveDeal;
            Terminal = terminal;
            ExpireDate = ContractDate.AddYears(Length);
            AutoRenew = autorenew;
            TerminalType = terminaltype;
        }

        #endregion

        #region Public Properties
        public int Id { get; set; }

        public Airline Airline { get; set; }
        public Airport Airport { get; set; }
        public bool AutoRenew { get; set; }
        public DateTime ContractDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsExclusiveDeal { get; set; }
        public TerminalType TerminalType { get; set; }
        public int Length { get; set; }
        public int MonthsLeft => GetMonthsLeft();
        public int NumberOfGates { get; set; }
        public bool PayFull { get; set; }
        public Terminal Terminal { get; set; }
        public ContractType Type { get; set; }
        public double YearlyPayment { get; set; }

        #endregion

        #region Public Methods and Operators
        public int GetMonthsLeft()
        {
            return MathHelpers.GetMonthsBetween(
                GameObject.GetInstance().GameTime,
                ContractDate.AddYears(Length));
        }

        #endregion
    }
}