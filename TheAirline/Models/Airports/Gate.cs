﻿using System;
using System.Collections.Generic;
using TheAirline.Models.Airlines;
using TheAirline.Models.General;

namespace TheAirline.Models.Airports
{
    // chs 11-04-11: changed for the possibility of extending a terminal
    //the class for a gate at an airport   
    public class Gate
    {
        #region Constructors and Destructors

        public Gate(DateTime deliveryDate)
        {
            DeliveryDate = deliveryDate;
        }
        #endregion

        #region Public Properties
        public int Id { get; set; }

        public Airline Airline { get; set; }
        public DateTime DeliveryDate { get; set; }

        #endregion

        //public Boolean HasRoute { get; set; }
        //public const int RoutesPerGate = 5;
    }

    // chs 11-04-11: changed for the possibility of extending a terminal
    //the collection of gates at an airport
    public class Gates 
    {
        #region Fields

        private List<Gate> _terminalGates;

        #endregion

        #region Methods

        private List<Gate> GetOrderedGates()
        {
            return
                _terminalGates.FindAll(
                    gate => gate.DeliveryDate > GameObject.GetInstance().GameTime);
        }

        #endregion

        #region Constructors and Destructors

        public Gates(int numberOfGates, DateTime deliveryDate, Airline airline)
        {
            _terminalGates = new List<Gate>();
            for (var i = 0; i < numberOfGates; i++)
            {
                var gate = new Gate(deliveryDate) {Airline = airline};

                _terminalGates.Add(gate);
            }
        }

        #endregion

        #region Public Properties

        public int NumberOfDeliveredGates => NumberOfGates - NumberOfOrderedGates;

        public int NumberOfGates => _terminalGates.Count;

        public int NumberOfOrderedGates => GetOrderedGates().Count;

        #endregion

        #region Public Methods and Operators

        public void AddGate(Gate gate)
        {
            _terminalGates.Add(gate);
        }

        public void Clear()
        {
            _terminalGates = new List<Gate>();
        }

        //returns all delivered gats
        public List<Gate> GetDeliveredGates()
        {
            return
                _terminalGates.FindAll(
                    gate => gate.DeliveryDate <= GameObject.GetInstance().GameTime);
        }

        // chs 11-07-11: changed for the possibility of extending a terminal
        //returns the ordered gates

        //returns the list of gates
        public List<Gate> GetGates()
        {
            return GetDeliveredGates();
        }

        #endregion

        //clears the gates
    }
}