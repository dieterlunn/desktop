﻿using System;
using TheAirline.Infrastructure.Enums;
using TheAirline.Models.Airports;

namespace TheAirline.Models.Routes
{
    //the class for a helicopter route
    // TODO - Move into its own file - Shouldn't be declared in the same file as another class
    // TODO - Subtype directly from Route. Possibly create enum for route type.
    public class HelicopterRoute : PassengerRoute
    {
        public HelicopterRoute(
            string id,
            Airport destination1,
            Airport destination2,
            DateTime startDate,
            double farePrice)
            : base(id, destination1, destination2, startDate, farePrice)
        {
            Type = RouteType.Helicopter;
        }
    }
}