﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using TheAirline.Infrastructure;
using TheAirline.Infrastructure.Enums;
using TheAirline.Models.Airports;
using TheAirline.Models.General.Statistics;

namespace TheAirline.Models.Routes
{
    //the class for a cargo route
    public class CargoRoute : Route
    {
        #region Constructors and Destructors

        public CargoRoute(
            string id,
            Airport destination1,
            Airport destination2,
            DateTime startDate,
            double pricePerUnit)
            : base(RouteType.Cargo, id, destination1, destination2, startDate)
        {
            PricePerUnit = pricePerUnit;
        }

        #endregion

        #region Public Properties

        [Versioning("unitprice")]
        public double PricePerUnit { get; set; }

        #endregion

        #region Public Methods and Operators
        public override double GetFillingDegree()
        {
            if (HasStopovers)
            {
                IEnumerable<Route> legs = Stopovers.SelectMany(s => s.Legs);
                Route[] enumerable = legs as Route[] ?? legs.ToArray();
                double fillingDegree = enumerable.Cast<CargoRoute>().Sum(leg => leg.GetFillingDegree());
                return fillingDegree/enumerable.Count();
            }
            double cargo = Convert.ToDouble(Statistics.GetTotalValue(StatisticsTypes.GetStatisticsType("Cargo")));

            double cargoCapacity =
                Convert.ToDouble(Statistics.GetTotalValue(StatisticsTypes.GetStatisticsType("Capacity")));

            if (cargo > cargoCapacity)
            {
                return 1;
            }

            return cargo/cargoCapacity;
        }

        #endregion
    }
}