﻿using System;

namespace TheAirline.Models.Airlines
{
    public class AirlineBudget
    {
        #region Public Properties

        public int Id { get; set; }
        public long AirportBudget { set; get; }
        public DateTime BudgetActive { set; get; }
        public DateTime BudgetExpires { set; get; }
        public long CsBudget { set; get; }
        public long Cash { set; get; }
        public long CompBudget { set; get; }
        public long EndYearCash { set; get; }
        public long EnginesBudget { set; get; }
        public long EquipmentBudget { set; get; }
        public int FleetSize { set; get; }
        public long FleetValue { set; get; }
        public long ItBudget { set; get; }
        public long InFlightBudget { set; get; }
        public long InternetBudget { set; get; }
        public long MaintenanceBudget { set; get; }
        public long MarketingBudget { set; get; }
        public long OverhaulBudget { set; get; }
        public long PrBudget { set; get; }
        public long PartsBudget { set; get; }
        public long PrintBudget { set; get; }
        public long PromoBudget { set; get; }
        public long RadioBudget { set; get; }
        public long RemainingBudget { set; get; }
        public long RemoteBudget { set; get; }
        public long SecurityBudget { set; get; }
        public long ServCenterBudget { set; get; }
        public int Subsidiaries { set; get; }
        public long TelevisionBudget { set; get; }
        public long TotalBudget { set; get; }
        public int TotalEmployees { set; get; }
        public int TotalPayroll { set; get; }
        public long TotalSubValue { set; get; }

        #endregion
    }
}