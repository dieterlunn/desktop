﻿using System.Collections.Generic;
using TheAirline.Helpers;
using TheAirline.Infrastructure.Enums;

namespace TheAirline.Models.Airlines
{
    /// <summary>
    /// This class is used for a type of Advertisement for an airline
    /// </summary>
    public class AdvertisementType
    {
        private double _aPrice;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="price"></param>
        /// <param name="reputationLevel"></param>
        public AdvertisementType(AirlineAdvertisementType type, string name, double price, int reputationLevel)
        {
            Type = type;
            Name = name;
            Price = price;
            ReputationLevel = reputationLevel;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public double Price
        {
            get { return GeneralHelpers.GetInflationPrice(_aPrice); }
            set { _aPrice = value; }
        }

        public int ReputationLevel { get; set; }

        public AirlineAdvertisementType Type { get; set; }
    }

/*! Airline advertisering types class.
* This class is used for the list of Advertisement types
* The class needs no parameters and construction since it is static
*/

    public class AdvertisementTypes
    {
        #region Static Fields

        private static readonly List<AdvertisementType> Advertisements = new List<AdvertisementType>();

        #endregion

        /*!returns the advertisement type for a specific type with a name
         */

        /*!adds a type to the list
         * */

        #region Public Methods and Operators

        public static void AddAdvertisementType(AdvertisementType type)
        {
            Advertisements.Add(type);
        }

        /*!clears the list of types
         */

        public static void Clear()
        {
            Advertisements.Clear();
        }

        public static AdvertisementType GetBasicType(AirlineAdvertisementType type)
        {
            return GetTypes(type).Find(t => t.ReputationLevel == 0);
        }

        public static AdvertisementType GetType(AirlineAdvertisementType type, string name)
        {
            return GetTypes(type).Find(t => t.Name == name);
        }

        /*!returns all advertisement types
         */

        public static List<AdvertisementType> GetTypes()
        {
            return Advertisements;
        }

        /*!returns the advertisement types for a specific type
         */

        public static List<AdvertisementType> GetTypes(AirlineAdvertisementType type)
        {
            return Advertisements.FindAll(t => t.Type == type);
        }

        #endregion
    }
}