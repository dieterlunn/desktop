﻿using System;
using System.Runtime.Serialization;
using TheAirline.Infrastructure;

namespace TheAirline.Models.General
{
    //the class for a period
    public class Period<T>
    {
        #region Constructors and Destructors

        public Period(T from, T to)
        {
            To = to;
            From = from;
        }
        #endregion

        #region Public Properties

        public int Id { get; set; }

        public T From { get; set; }
        public T To { get; set; }

        #endregion
    }
}