﻿using System.Collections.Generic;
using System.IO;
using TheAirline.Infrastructure;

namespace TheAirline.Models.General.Countries.Towns
{
    //the class for a state
    public class State
    {
        private string _flag;

        #region Constructors and Destructors

        public State(Country country, string name, string shortname, bool overseas)
        {
            Country = country;
            Name = name;
            ShortName = shortname;
            IsOverseas = overseas;
        }

        #endregion

        #region Public Properties

        public int Id { get; set; }

        public Country Country { get; set; }

        public string Flag
        {
            get
            {
                if (!File.Exists(_flag))
                {
                    Flag = AppSettings.GetDataPath() + "\\graphics\\flags\\" + Name + ".png";
                }
                return _flag;
            }
            set { _flag = value; }
        }

        public bool IsOverseas { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }

        #endregion
    }

    //the list of states
    public class States
    {
        #region Static Fields

        private static readonly List<State> _states = new List<State>();

        #endregion

        //adds a state to the list

        #region Public Methods and Operators

        public static void AddState(State state)
        {
            _states.Add(state);
        }

        //clears the list of states
        public static void Clear()
        {
            _states.Clear();
        }

        //returns a state with a short name and from a country
        public static State GetState(Country country, string shortname)
        {
            return _states.Find(s => s.Country == country && s.ShortName == shortname);
        }

        #endregion
    }
}