﻿using System;
using Microsoft.EntityFrameworkCore;
using TheAirline.Models.Airlines;
using TheAirline.Models.Airports;
using TheAirline.Models.General;
using TheAirline.Models.General.Countries;
using TheAirline.Models.General.Countries.Towns;
using Settings = TheAirline.Models.General.Settings;

namespace TheAirline.Db
{
    public sealed class AirlineContext : DbContext
    {
        public DbSet<Settings> Settings { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Continent> Continents { get; set; }
        public DbSet<Difficulty> Difficulties { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<AirlineBudget> AirlineBudgets { get; set; }
        public DbSet<AdvertisementType> AdvertisementTypes { get; set; }
        public DbSet<Period<DateTime>> Periods { get; set; }
        public DbSet<Town> Towns { get; set; }
        public DbSet<State> States { get; set; }
        //public DbSet<Gate> Gates { get; set; }
        //public DbSet<Terminal> Terminals { get; set; }
        public DbSet<CountryTailNumber> TailNumbers { get; set; }
        public DbSet<Country> Countries { get; set; }
        //public DbSet<Airline> Airlines { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlCe(@"Data Source=Airline.sdf");
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}
