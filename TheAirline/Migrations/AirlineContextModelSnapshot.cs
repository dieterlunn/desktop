﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TheAirline.Db;

namespace TheAirline.Migrations
{
    [DbContext(typeof(AirlineContext))]
    partial class AirlineContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("TheAirline.Models.Airlines.AdvertisementType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<double>("Price");

                    b.Property<int>("ReputationLevel");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("AdvertisementTypes");
                });

            modelBuilder.Entity("TheAirline.Models.Airlines.AirlineBudget", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AirportBudget");

                    b.Property<DateTime>("BudgetActive");

                    b.Property<DateTime>("BudgetExpires");

                    b.Property<long>("Cash");

                    b.Property<long>("CompBudget");

                    b.Property<long>("CsBudget");

                    b.Property<long>("EndYearCash");

                    b.Property<long>("EnginesBudget");

                    b.Property<long>("EquipmentBudget");

                    b.Property<int>("FleetSize");

                    b.Property<long>("FleetValue");

                    b.Property<long>("InFlightBudget");

                    b.Property<long>("InternetBudget");

                    b.Property<long>("ItBudget");

                    b.Property<long>("MaintenanceBudget");

                    b.Property<long>("MarketingBudget");

                    b.Property<long>("OverhaulBudget");

                    b.Property<long>("PartsBudget");

                    b.Property<long>("PrBudget");

                    b.Property<long>("PrintBudget");

                    b.Property<long>("PromoBudget");

                    b.Property<long>("RadioBudget");

                    b.Property<long>("RemainingBudget");

                    b.Property<long>("RemoteBudget");

                    b.Property<long>("SecurityBudget");

                    b.Property<long>("ServCenterBudget");

                    b.Property<int>("Subsidiaries");

                    b.Property<long>("TelevisionBudget");

                    b.Property<long>("TotalBudget");

                    b.Property<int>("TotalEmployees");

                    b.Property<int>("TotalPayroll");

                    b.Property<long>("TotalSubValue");

                    b.HasKey("Id");

                    b.ToTable("AirlineBudgets");
                });

            modelBuilder.Entity("TheAirline.Models.General.Countries.Continent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("Uid");

                    b.HasKey("Id");

                    b.ToTable("Continents");
                });

            modelBuilder.Entity("TheAirline.Models.General.Countries.Country", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Flag");

                    b.Property<string>("IsoName");

                    b.Property<string>("Name");

                    b.Property<int?>("RegionId");

                    b.Property<string>("ShortName");

                    b.Property<string>("TailNumberFormat");

                    b.Property<int?>("TailNumberKey");

                    b.Property<string>("Uid");

                    b.HasKey("Id");

                    b.HasIndex("RegionId");

                    b.HasIndex("TailNumberKey")
                        .IsUnique();

                    b.ToTable("Countries");
                });

            modelBuilder.Entity("TheAirline.Models.General.Countries.CountryTailNumber", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LastTailNumber");

                    b.HasKey("Id");

                    b.ToTable("TailNumbers");
                });

            modelBuilder.Entity("TheAirline.Models.General.Countries.Currency", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CountryId");

                    b.Property<double>("Exchange");

                    b.Property<DateTime>("From");

                    b.Property<string>("Name");

                    b.Property<string>("Symbol");

                    b.Property<bool>("SymbolOnRight");

                    b.Property<DateTime>("To");

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.ToTable("Currencies");
                });

            modelBuilder.Entity("TheAirline.Models.General.Countries.Region", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ContinentId");

                    b.Property<double>("FuelIndex");

                    b.Property<string>("Name");

                    b.Property<string>("Uid");

                    b.HasKey("Id");

                    b.HasIndex("ContinentId");

                    b.ToTable("Regions");
                });

            modelBuilder.Entity("TheAirline.Models.General.Countries.Towns.Town", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("Towns");
                });

            modelBuilder.Entity("TheAirline.Models.General.Difficulty", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("AiLevel");

                    b.Property<double>("LoanLevel");

                    b.Property<double>("MoneyLevel");

                    b.Property<string>("Name");

                    b.Property<double>("PassengersLevel");

                    b.Property<double>("PriceLevel");

                    b.Property<double>("StartDataLevel");

                    b.HasKey("Id");

                    b.ToTable("Difficulties");
                });

            modelBuilder.Entity("TheAirline.Models.General.Period<System.DateTime>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("From");

                    b.Property<DateTime>("To");

                    b.HasKey("Id");

                    b.ToTable("Periods");
                });

            modelBuilder.Entity("TheAirline.Models.General.Player", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ContinentId");

                    b.Property<int?>("DifficultyId");

                    b.Property<int>("Focus");

                    b.Property<bool>("MajorAirports");

                    b.Property<int>("NumOfOpponents");

                    b.Property<bool>("Paused");

                    b.Property<bool>("RandomOpponents");

                    b.Property<bool>("RealData");

                    b.Property<int?>("RegionId");

                    b.Property<bool>("SameRegion");

                    b.Property<int>("StartYear");

                    b.Property<bool>("UseDays");

                    b.HasKey("Id");

                    b.HasIndex("ContinentId");

                    b.HasIndex("DifficultyId");

                    b.HasIndex("RegionId");

                    b.ToTable("Players");
                });

            modelBuilder.Entity("TheAirline.Models.General.Settings", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AirportCodeDisplay");

                    b.Property<int>("AutoSave");

                    b.Property<int>("ClearStats");

                    b.Property<bool>("CurrencyShorten");

                    b.Property<int?>("DifficultyDisplayId");

                    b.Property<int>("GameSpeed");

                    b.Property<string>("Language");

                    b.Property<bool>("MailsOnAirlineRoutes");

                    b.Property<bool>("MailsOnBadWeather");

                    b.Property<bool>("MailsOnLandings");

                    b.Property<int>("Mode");

                    b.HasKey("Id");

                    b.HasIndex("DifficultyDisplayId");

                    b.ToTable("Settings");
                });

            modelBuilder.Entity("TheAirline.Models.General.Countries.Country", b =>
                {
                    b.HasOne("TheAirline.Models.General.Countries.Region", "Region")
                        .WithMany()
                        .HasForeignKey("RegionId");

                    b.HasOne("TheAirline.Models.General.Countries.CountryTailNumber", "TailNumbers")
                        .WithOne("Country")
                        .HasForeignKey("TheAirline.Models.General.Countries.Country", "TailNumberKey");
                });

            modelBuilder.Entity("TheAirline.Models.General.Countries.Currency", b =>
                {
                    b.HasOne("TheAirline.Models.General.Countries.Country")
                        .WithMany("Currencies")
                        .HasForeignKey("CountryId");
                });

            modelBuilder.Entity("TheAirline.Models.General.Countries.Region", b =>
                {
                    b.HasOne("TheAirline.Models.General.Countries.Continent", "Continent")
                        .WithMany("Regions")
                        .HasForeignKey("ContinentId");
                });

            modelBuilder.Entity("TheAirline.Models.General.Player", b =>
                {
                    b.HasOne("TheAirline.Models.General.Countries.Continent", "Continent")
                        .WithMany()
                        .HasForeignKey("ContinentId");

                    b.HasOne("TheAirline.Models.General.Difficulty", "Difficulty")
                        .WithMany()
                        .HasForeignKey("DifficultyId");

                    b.HasOne("TheAirline.Models.General.Countries.Region", "Region")
                        .WithMany()
                        .HasForeignKey("RegionId");
                });

            modelBuilder.Entity("TheAirline.Models.General.Settings", b =>
                {
                    b.HasOne("TheAirline.Models.General.Difficulty", "DifficultyDisplay")
                        .WithMany()
                        .HasForeignKey("DifficultyDisplayId");
                });
        }
    }
}
