﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheAirline.Migrations
{
    public partial class AddAirlineBudgets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AirlineBudgets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlCe:ValueGeneration", "True"),
                    AirportBudget = table.Column<long>(nullable: false),
                    BudgetActive = table.Column<DateTime>(nullable: false),
                    BudgetExpires = table.Column<DateTime>(nullable: false),
                    Cash = table.Column<long>(nullable: false),
                    CompBudget = table.Column<long>(nullable: false),
                    CsBudget = table.Column<long>(nullable: false),
                    EndYearCash = table.Column<long>(nullable: false),
                    EnginesBudget = table.Column<long>(nullable: false),
                    EquipmentBudget = table.Column<long>(nullable: false),
                    FleetSize = table.Column<int>(nullable: false),
                    FleetValue = table.Column<long>(nullable: false),
                    InFlightBudget = table.Column<long>(nullable: false),
                    InternetBudget = table.Column<long>(nullable: false),
                    ItBudget = table.Column<long>(nullable: false),
                    MaintenanceBudget = table.Column<long>(nullable: false),
                    MarketingBudget = table.Column<long>(nullable: false),
                    OverhaulBudget = table.Column<long>(nullable: false),
                    PartsBudget = table.Column<long>(nullable: false),
                    PrBudget = table.Column<long>(nullable: false),
                    PrintBudget = table.Column<long>(nullable: false),
                    PromoBudget = table.Column<long>(nullable: false),
                    RadioBudget = table.Column<long>(nullable: false),
                    RemainingBudget = table.Column<long>(nullable: false),
                    RemoteBudget = table.Column<long>(nullable: false),
                    SecurityBudget = table.Column<long>(nullable: false),
                    ServCenterBudget = table.Column<long>(nullable: false),
                    Subsidiaries = table.Column<int>(nullable: false),
                    TelevisionBudget = table.Column<long>(nullable: false),
                    TotalBudget = table.Column<long>(nullable: false),
                    TotalEmployees = table.Column<int>(nullable: false),
                    TotalPayroll = table.Column<int>(nullable: false),
                    TotalSubValue = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AirlineBudgets", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AirlineBudgets");
        }
    }
}
