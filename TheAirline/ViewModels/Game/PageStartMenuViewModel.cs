﻿using System;
using System.ComponentModel.Composition;
using MaterialDesignThemes.Wpf;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using TheAirline.Infrastructure.Events;
using TheAirline.Views.Game;

namespace TheAirline.ViewModels.Game
{
    [Export]
    public class PageStartMenuViewModel : BindableBase
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IRegionManager _regionManager;

        [ImportingConstructor]
        public PageStartMenuViewModel(IRegionManager regionManager, IEventAggregator eventAggregator)
        {
            _regionManager = regionManager;
            _eventAggregator = eventAggregator;
            NavigateCommand = new DelegateCommand<Uri>(Navigate);
            ExitCommand = new DelegateCommand(ExitGame);
        }

        public DelegateCommand<Uri> NavigateCommand { get; }
        public DelegateCommand ExitCommand { get; }
        public Uri NewGameUri => new Uri("/PageStartData", UriKind.Relative);
        public Uri LoadGameUri => new Uri("/PageLoadGame", UriKind.Relative);
        public Uri SettingsUri => new Uri("/PageSettings", UriKind.Relative);
        public Uri CreditsUri => new Uri("/PageCredits", UriKind.Relative);

        private void Navigate(Uri view)
        {
            _regionManager.RequestNavigate("MainContentRegion", view);
        }

        private void ExitGame()
        {
            _eventAggregator.GetEvent<CloseGameEvent>().Publish(null);
        }
    }
}