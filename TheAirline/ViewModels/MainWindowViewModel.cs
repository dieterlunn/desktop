﻿using System.ComponentModel.Composition;
using System.Windows.Input;
using Prism.Events;
using Prism.Mvvm;
using TheAirline.GraphicsModel.UserControlModel.MessageBoxModel;
using TheAirline.Infrastructure.Events;

namespace TheAirline.ViewModels
{
    [Export]
    public class MainWindowViewModel : BindableBase
    {
        private readonly DelegateCommand _closeCommand;
        private readonly IEventAggregator _events;

        [ImportingConstructor]
        public MainWindowViewModel(IEventAggregator eventAggregator)
        {
            _events = eventAggregator;
            _closeCommand = new DelegateCommand(CloseGame);
        }

        public ICommand CloseCommand => _closeCommand;

        public void CloseGame()
        {
            _events.GetEvent<CloseGameEvent>().Publish(null);
        }
    }
}