﻿namespace TheAirline.Infrastructure.Enums
{
    public enum AirlineLicense
    {
        Domestic,
        Regional,
        ShortHaul,
        LongHaul
    }
}