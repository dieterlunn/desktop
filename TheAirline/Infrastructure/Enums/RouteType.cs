﻿namespace TheAirline.Infrastructure.Enums
{
    public enum RouteType
    {
        Cargo,
        Mixed,
        Passenger,
        Helicopter
    }
}