﻿namespace TheAirline.Infrastructure.Enums
{
    public enum AirportType
    {
        LongHaulInternational,
        Regional,
        Domestic,
        ShortHaulInternational
    }
}