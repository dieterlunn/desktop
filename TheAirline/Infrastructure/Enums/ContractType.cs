﻿namespace TheAirline.Infrastructure.Enums
{
    public enum ContractType
    {
        Full,
        FullService,
        MediumService,
        LowService
    }
}