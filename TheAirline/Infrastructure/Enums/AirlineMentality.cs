﻿namespace TheAirline.Infrastructure.Enums
{
    public enum AirlineMentality
    {
        Safe,
        Moderate,
        Aggressive
    }
}