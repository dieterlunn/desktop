﻿namespace TheAirline.Infrastructure.Enums
{
    public enum TerminalType
    {
        Cargo,
        Passenger
    }
}