﻿namespace TheAirline.Infrastructure.Enums
{
    public enum AirlineAdvertisementType
    {
        Newspaper = 1930,
        Radio = 1940,
        Tv = 1950,
        Internet = 1995
    }
}