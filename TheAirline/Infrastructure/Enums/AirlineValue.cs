﻿namespace TheAirline.Infrastructure.Enums
{
    public enum AirlineValue
    {
        VeryLow,
        Low,
        Normal,
        High,
        VeryHigh
    }
}